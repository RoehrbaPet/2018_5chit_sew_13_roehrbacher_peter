﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace TdoT_WPF
{
    public class MainWindowVM : AViewModel
    {
        public ObservableCollection<Room> Rooms { get; private set; }
        public ObservableCollection<StationVM> Stations { get; private set; }
        public ObservableCollection<Student> Students { get; private set; }
        public ObservableCollection<Teacher> Teachers { get; private set; }
        public ObservableCollection<Person> AssignedPeople { get; private set; }

        public Student SelectedStudent { get; set; }
        public Teacher SelectedTeacher { get; set;  }
        public StationVM SelectedStation { get; set; }
        private StationVM newStation;
        public StationVM NewStation {
            get
        {
            return newStation;
        }

        set
            {
                newStation = value;
                InvokePropertyChanged();
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return new RelayCommand(this._AddStation, o => NewStation.Description!=""&&NewStation.Name!=""&&NewStation.Room!=null);
            }
            set { AddCommand = value; }
        }

        public RelayCommand EditCommand
        {
            get
            {
                return new RelayCommand(this._EditStation,o=>SelectedStation!= null);
                
            }
            set { EditCommand = value; }
        }

        public RelayCommand AssignStudent
        {
            get
            {
                return new RelayCommand(this._AssignStudent, o=> SelectedStudent!= null && SelectedStation!= null);
            }
        }

        public RelayCommand AssignTeacher

        {
            get
            {
                return new RelayCommand(this._AssignTeacher, o=> SelectedTeacher!= null && SelectedStation!= null);
            }
        }
        public RelayCommand AssignStudentCommand;
        public RelayCommand AssignTeacherCommand;

        public MainWindowVM()
        {
            this.Stations = new ObservableCollection<StationVM>();
            this.Rooms = new ObservableCollection<Room>(Room.Read());
            this.Students = new ObservableCollection<Student>(Student.Read());
            this.Teachers = new ObservableCollection<Teacher>(Teacher.Read());
            this.AssignedPeople = new ObservableCollection<Person>();
            this.NewStation = new StationVM(new Station());
        }

        private void _AddStation(object sender) {
            Stations.Add(NewStation);
            NewStation = new StationVM(new Station());
        }
        
        private void _EditStation(object sender)
        {

            EditStationWindow window = new EditStationWindow(SelectedStation);
            window.Show();
        }

        private void _AssignStudent(object sender)
        {
            SelectedStudent.Station = SelectedStation.Station;
            AssignedPeople.Add(SelectedStudent);
            Students.Remove(SelectedStudent);
        }

        private void _AssignTeacher(object sender)
        {
            SelectedTeacher.Station = SelectedStation.Station;
            AssignedPeople.Add(SelectedTeacher);
            Teachers.Remove(SelectedTeacher);
        }
    }
}
