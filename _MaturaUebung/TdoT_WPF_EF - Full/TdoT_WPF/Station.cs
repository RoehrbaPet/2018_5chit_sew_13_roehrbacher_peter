﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{
    public class Station
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Room Room { get; set; }
        public int Index { get; set; }

        public virtual List<Person> AssignedPersons { get; set; } = new
            List<Person>();
    }
}
