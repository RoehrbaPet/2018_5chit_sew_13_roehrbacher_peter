﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TdoT_WPF;

namespace TdoT_WPF
{
    public class TdoTContext : DbContext
    {
        #region Singleton
        private static TdoTContext context;

        private TdoTContext() : base("TdoT_DB2")
        {
            Database.SetInitializer(new TdoTInitializer());
        }

        public static TdoTContext Context
        {
            get
            {
                if(context == null)
                    context = new TdoTContext();
                return context;
            }
            protected set { context = value; }
        }
        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Station> Stations { get; set; }
    }

    

}
