﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{

   public abstract class ARepository<TEntity, TId> : IDisposable
        where TEntity : class, new()
    {
        protected TdoTContext Context { get; private set; }
        protected abstract DbSet<TEntity> Table { get; set; }

        public ARepository(TdoTContext context)
        {
            this.Context = context;
        }

        public List<TEntity> GetAll() => Table.ToList();
        public TEntity GetOne(TId id) => Table.Find(id);

        public int Add(TEntity entity)
        {
            Table.Add(entity);
            return Context.SaveChanges();
        }

        public int AddRange(IEnumerable<TEntity> entities)
        {
            Table.AddRange(entities);
            return Context.SaveChanges();
        }

        public int Delete(TEntity entity)
        {
            Table.Remove(entity);
            return Context.SaveChanges();


        }

        public int DeleteRange(IEnumerable<TEntity> entities)
        {
            Table.RemoveRange(entities);
            return Context.SaveChanges();
        }

        public int Update() => Context.SaveChanges();
        public void Dispose() => Context.Dispose();
    }
}
    
