﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TdoT_WPF
{
    /// <summary>
    /// Interaktionslogik für EditStationWindow.xaml
    /// </summary>
    public partial class EditStationWindow : Window
    {
        public EditStationWindow(StationVM Selected)
        {
            
            InitializeComponent();
            scp_whole.DataContext = new EditStationVM(Selected);
            this.Show();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            TdoTContext.Context.SaveChanges();

            base.OnClosing(e);
        }
    }
}
