﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace TdoT_WPF
{
    public class MainWindowVM : AViewModel
    {
        public ObservableCollection<Room> Rooms { get; private set; }
        public ObservableCollection<StationVM> Stations { get; private set; }
        public ObservableCollection<Student> Students { get; private set; }
        public ObservableCollection<Teacher> Teachers { get; private set; }
        public ObservableCollection<Person> AssignedPeople { get; private set; }
        private ObservableCollection<object> _result;

        public ObservableCollection<object> QueryResult
        {
            get { return _result; }
            private set
            {
                _result = value;
                InvokePropertyChanged();
            }
        }

        public ObservableCollection<Person> Types { get; set; }
        public Person SelectedPersonType { get; set; }

        private StationRepository _repository = new StationRepository(TdoTContext.Context);
        public Student SelectedStudent { get; set; }
        public Teacher SelectedTeacher { get; set;  }
        public StationVM SelectedStation { get; set; }
        public StationVM SelectedStationInfo { get; set; }
        public StationVM SelectedStationAss { get; set; }
        private StationVM _newStation;
        public StationVM NewStation {
            get
        {
            return _newStation;
        }

        set
            {
                _newStation = value;
                InvokePropertyChanged();
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return new RelayCommand(this._AddStation, o => NewStation.Description!=""&&NewStation.Name!=""&&NewStation.Room!=null);
            }
            set { AddCommand = value; }
        }

        public RelayCommand EditCommand
        {
            get
            {
                return new RelayCommand(this._EditStation,o=>SelectedStation!= null);
                
            }
            set { EditCommand = value; }
        }

        public RelayCommand AssignStudent
        {
            get
            {
                return new RelayCommand(this._AssignStudent, o=> SelectedStudent!= null && SelectedStationAss!= null);
            }
        }

        public RelayCommand AssignTeacher

        {
            get
            {
                return new RelayCommand(this._AssignTeacher, o=> SelectedTeacher!= null && SelectedStationAss!= null);
            }
        }

        public RelayCommand GetStudentCounts
        {
            get
            {
                return new RelayCommand(this._StudentCount, o=> true);
            }
        }

        public RelayCommand GetPersons
        {
            get
            {
                return new RelayCommand(this._PersonCount, o=> SelectedStationInfo != null && SelectedPersonType != null);
            }
        }


        public MainWindowVM()
        {
            //local must be prepopulated manually
            TdoTContext.Context.Students.Load();
            TdoTContext.Context.Teachers.Load();
            TdoTContext.Context.Rooms.Load();
            TdoTContext.Context.Stations.Load();
            this.Stations = new ObservableCollection<StationVM>(TdoTContext.Context.Stations.Local.Select(s => new StationVM(s)));
            this.Rooms = TdoTContext.Context.Rooms.Local;
            //new collection is initialized, because otherwise teachers and students cannot be removed from the listbox
            this.Students = new ObservableCollection<Student>(TdoTContext.Context.Students.Local);
            this.Teachers = new ObservableCollection<Teacher>(TdoTContext.Context.Teachers.Local);
            this.AssignedPeople = new ObservableCollection<Person>();
            this.NewStation = new StationVM(new Station());
            _PopulateTypes();
        }

        private void _PopulateTypes()
        {
            Types = new ObservableCollection<Person>() {new Student() {Firstname = "Schüler"}, new Teacher() { Firstname = "Lehrer"}, new Person() { Firstname = "Beliebig"} };
            
        }

        private void _AddStation(object sender) {
            Stations.Add(NewStation);
            
            NewStation = new StationVM(new Station());
            TdoTContext.Context.Stations.Add(NewStation.Station);
            try
            {
                TdoTContext.Context.SaveChanges();

            }
            catch (DbEntityValidationException e)
            {
                MessageBox.Show("Bitte füllen Sie alle Felder aus");
            }
        }
        
        private void _EditStation(object sender)
        {

            EditStationWindow window = new EditStationWindow(SelectedStation);
            window.Show();
        }

        private void _AssignStudent(object sender)
        {
            SelectedStudent.Station = SelectedStationAss.Station;
            AssignedPeople.Add(SelectedStudent);
            TdoTContext.Context.Students.Find(SelectedStudent.Id).Station = SelectedStationAss.Station;
            Students.Remove(SelectedStudent);

            TdoTContext.Context.SaveChanges();

        }

        private void _AssignTeacher(object sender)
        {
            SelectedTeacher.Station = SelectedStationAss.Station;
            AssignedPeople.Add(SelectedTeacher);
            TdoTContext.Context.Teachers.Find(SelectedTeacher.Id).Station = SelectedStationAss.Station;

            Teachers.Remove(SelectedTeacher);
            TdoTContext.Context.SaveChanges();

        }

        private void _StudentCount(object sender)
        {
            QueryResult = new ObservableCollection<object>(_repository.GetStudentCountForAllStations());
        }

        private void _PersonCount(object sender)
        {
            
            if (SelectedPersonType is Student)
            {
                QueryResult = new ObservableCollection<object>(_repository.GetPersonsOfStationByType<Student>(SelectedStationInfo.Station));

            }
            else if(SelectedPersonType is Teacher)
            {
                QueryResult = new ObservableCollection<object>(_repository.GetPersonsOfStationByType<Teacher>(SelectedStationInfo.Station));

            }

            else if (SelectedPersonType is Person)
            {

                QueryResult = new ObservableCollection<object>(_repository.GetPersonsOfStationByType<Person>(SelectedStationInfo.Station));

            }

        }
    }
}
