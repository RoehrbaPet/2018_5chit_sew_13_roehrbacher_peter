﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{


    public class EditStationVM : AViewModel
    {
        public StationVM Station { get; private set; }
        public ObservableCollection<Room> Rooms { get; private set; }
        public ObservableCollection<Station> Stations { get; private set; }
        public int Id {
            get { return Station.Id; }
            set {
                Station.Id = value;
                InvokePropertyChanged();
            }
        }
        public string Name {
            get { return Station.Name; }
            set {
                Station.Name = value;
                InvokePropertyChanged();
            }
        }
        public string Description {
            get { return Station.Description; }
            set {
                Station.Description = value;
                InvokePropertyChanged();
            }
        }
        public Room Room
        {
            get
            {
                Room room = this.Rooms.Where(x => x.Id == Station.Room.Id).First();
                return room;
            }
            set {
                Station.Room = value;
                InvokePropertyChanged();
            }
        }

        public int Index
        {
            get { return Station.Index;}
            set
            {
                Station.Index = value;
                InvokePropertyChanged();
            }
        }

        public EditStationVM() : this(new StationVM()) { }
        public EditStationVM(StationVM station)
        {
            this.Station = station;
            this.Rooms = new ObservableCollection<Room>(Room.Read());
        }
    }
}


