﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{
    public class StationVM : AViewModel
    {
        private Station station;
        public Station Station {
            get { return station; }
            private set
            {
                station = value;
                InvokePropertyChanged();
            } }

        public int Id
        {
            get { return Station.Id; }
            set
            {
                Station.Id = value;
                InvokePropertyChanged();
            }
        }
        public string Name
        {
            get { return Station.Name; }
            set
            {
                Station.Name = value;
                InvokePropertyChanged();
            }
        }
        public string Description
        {
            get { return Station.Description; }
            set
            {
                Station.Description = value;
                InvokePropertyChanged();
            }
        }
        public Room Room
        {
            get { return Station.Room; }
            set
            {
                Station.Room = value;
                InvokePropertyChanged();
            }
        }

        public int Index {
            get { return Station.Index; }
            set {
                Station.Index = value;
                InvokePropertyChanged();
            }
        }

        public StationVM() : this(new Station()) { }
        public StationVM(Station station)
        {
            this.Station = station;
        }

        public override string ToString()
        {
            return Station.ToString();
        }
    }
}
