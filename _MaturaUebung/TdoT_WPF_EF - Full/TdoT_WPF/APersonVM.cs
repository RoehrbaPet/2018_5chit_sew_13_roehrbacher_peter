﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{
    public abstract class APersonVM : AViewModel
    {
        protected Person person;
        public int Id
        {
            get
            {
               return person.Id;
            }
            set
            {
                person.Id = value;
                InvokePropertyChanged();
            }
        }
        public string Firstname
        {
            get { return person.Firstname; }
            set
            {
                person.Firstname = value;
                InvokePropertyChanged();
            }
        }
        public string Lastname
        {
            get
            {
                return person.Lastname;
            }
            set
            {
                person.Lastname = value;
                InvokePropertyChanged();
            }
        }

        public APersonVM(): this(new Person()) { }
        public APersonVM(Person person)
        {
            this.person = person;
        }
    }
}
