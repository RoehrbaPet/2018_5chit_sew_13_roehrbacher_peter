﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TdoT_WPF;

namespace TdoT_WPF
{
    class TdoTInitializer : DropCreateDatabaseAlways<TdoTContext>
    {
        protected override void Seed(TdoTContext context)
        {
            base.Seed(context);

            List<Teacher> teachers = Teacher.Read();
            List<Room> rooms = Room.Read();
            List<Student> students = Student.Read();
            List<Station> stations = new List<Station>();
            stations.Add(new Station()
            {
                Description = "SEW Spiele",
                Index = 2,
                Name = "SEW",
                Room = rooms[2],
                AssignedPersons = new List<Person>()
                    { teachers[3],
                       students[2],
                       students[1]}
            });
            stations.Add(new Station()
            {
                Name= "Photoshop",
                Description = "Videobearbeitung",
                Index = 1,
                Room = rooms[1],
                AssignedPersons = new List<Person>()
                { teachers[2],
                   students[3],
                    students[0]}
            });
            context.Stations.AddRange(stations);
            context.Students.AddRange(students);
            context.Teachers.AddRange(teachers);
            context.Rooms.AddRange(rooms);
            context.SaveChanges();
        }
    }
}