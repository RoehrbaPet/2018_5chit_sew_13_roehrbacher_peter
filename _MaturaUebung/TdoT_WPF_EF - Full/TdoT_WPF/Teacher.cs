﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{
    public partial class Teacher: Person
    {
        public List<string> Subjects { get; set; }
        //public Station Station { get; set; }
    }

    public partial class Teacher
    {
        public static List<Teacher> Read()
        {
            List<Teacher> res = new List<Teacher>();

            foreach (string cur in File.ReadAllLines("teacher.csv", Encoding.Default))
            {
                string[] parts = cur.Split(';');
                Teacher t = new Teacher();

                t.Id = Int32.Parse(parts[0]);
                t.Firstname = parts[1];
                t.Lastname = parts[2];
                t.Subjects = parts.ToList().GetRange(3, parts.Length - 3);

                res.Add(t);
            }

            return res;
        }
    }
}
