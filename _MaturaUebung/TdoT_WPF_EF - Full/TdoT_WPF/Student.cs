﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdoT_WPF
{
    public partial class Student: Person
    {
        public string Schoolclass { get; set; } //2Bhit
        //public Station Station { get; set; }
       
    }

    public partial class Student
    {
        public static List<Student> Read()
        {
            List<Student> res = new List<Student>();

            foreach (string cur in File.ReadAllLines("students.csv", Encoding.Default))
            {
                string[] parts = cur.Split(';');
                Student s = new Student();

                s.Id = Int32.Parse(parts[0]);
                s.Firstname = parts[1];
                s.Lastname = parts[2];
                s.Schoolclass = parts[3];

                res.Add(s);
            }

            return res;
        }
    }
}
