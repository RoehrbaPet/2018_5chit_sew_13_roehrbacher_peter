﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Name_ORM_Abschnitt3
{
    public partial class Room
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
    }

    public partial class Room
    {
        public static List<Room> Read()
        {
            List<Room> res = new List<Room>();

            foreach (string cur in File.ReadAllLines("rooms.csv", Encoding.Default))
            {
                string[] parts = cur.Split(';');
                Room r = new Room();

                r.Id = Int32.Parse(parts[0]);
                r.Description = parts[1];
                r.Location = parts[2];

                res.Add(r);
            }

            return res;
        }

    }
}
