﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Name_ORM_Abschnitt3
{
    public class Station
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Room Room { get; set; }
        public int Index { get; set; }

        public virtual List<Person> AssignedPersons { get; set; }
    }
}
