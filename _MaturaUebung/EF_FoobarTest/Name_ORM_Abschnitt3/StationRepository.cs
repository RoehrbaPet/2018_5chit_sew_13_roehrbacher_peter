﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Name_ORM_Abschnitt3
{
   public class StationRepository:ARepository<Station, int>
    {
        public StationRepository(TdoTContext context) : base(context)
        {
            this.Table = context.Stations;
        }

        protected override DbSet<Station> Table { get; set; }

        public List<StudentCountPerStation> GetStudentCountForAllStations()
        {
            //apparently san anonyme typen huren
            //se ficken nämlich
            //return Table.Select(s => new { Station = s, StudentCount = filterStudents(s.AssignedPersons).Count}).ToList();
            return Table.Select(s => new StudentCountPerStation()
                {Station = s, StudentCount = s.AssignedPersons.OfType<Student>().Count()}).ToList();
        }
        //was used for anonymous type -> deprecated
        private List<Student> filterStudents(List<Person> list)
        {
            return list.OfType<Student>().ToList();
        }
        public List<T> GetPersonsOfStationByType<T>(Station s)
        {
            return Table.Local.Where(x => x == s).Single().AssignedPersons.OfType<T>().ToList();
        }


    }

    public class StudentCountPerStation
    {
        public int StudentCount { get; set; }
        public Station Station { get; set; }

    }
}
