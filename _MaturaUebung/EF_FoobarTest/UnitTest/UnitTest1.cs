﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using System.Collections.Generic;
using Name_ORM_Abschnitt3;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class StationRepositoryTest
    {
        public StationRepository repo = new StationRepository(TdoTContext.Context); 
        [TestMethod]
        public void TestStudentCount()
        {
            List<StudentCountPerStation> list = repo.GetStudentCountForAllStations();
            Assert.AreEqual(list[0].StudentCount, 2);
            Assert.AreEqual(list[1].StudentCount, 2);
        }

        [TestMethod]
        public void TestPersonsByType()
        {
            List<Teacher> list = repo.GetPersonsOfStationByType<Teacher>(repo.GetOne(1));
            Assert.AreEqual(1, list.Count);
        }
    }
}
