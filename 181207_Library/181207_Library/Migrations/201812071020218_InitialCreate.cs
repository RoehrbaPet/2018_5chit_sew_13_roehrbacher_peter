namespace _181207_Library.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
           
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false),
                        City = c.String(),
                        Street = c.String(),
                        ZipCode = c.String(),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.NormalBuyers", t => t.AddressId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.ABuyers",
                c => new
                    {
                        BuyerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.BuyerId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Article_ArticleId = c.Int(),
                        Customer_BuyerId = c.Int(),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.AArticles", t => t.Article_ArticleId)
                .ForeignKey("dbo.ABuyers", t => t.Customer_BuyerId)
                .Index(t => t.Article_ArticleId)
                .Index(t => t.Customer_BuyerId);
            
            CreateTable(
                "dbo.AArticles",
                c => new
                    {
                        ArticleId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Category = c.String(),
                        ArticleType = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ArticleId);
            
            CreateTable(
                "dbo.APublishers",
                c => new
                    {
                        PublisherId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PublisherType = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.PublisherId);
            
            CreateTable(
                "dbo.Schools",
                c => new
                    {
                        SchoolId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SchoolId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        teacher_BuyerId = c.Int(),
                    })
                .PrimaryKey(t => t.SubjectId)
                .ForeignKey("dbo.Teachers", t => t.teacher_BuyerId)
                .Index(t => t.teacher_BuyerId);
            
            CreateTable(
                "dbo.AArticleAPublishers",
                c => new
                    {
                        AArticle_ArticleId = c.Int(nullable: false),
                        APublisher_PublisherId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AArticle_ArticleId, t.APublisher_PublisherId })
                .ForeignKey("dbo.AArticles", t => t.AArticle_ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.APublishers", t => t.APublisher_PublisherId, cascadeDelete: true)
                .Index(t => t.AArticle_ArticleId)
                .Index(t => t.APublisher_PublisherId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        BuyerId = c.Int(nullable: false),
                        school_SchoolId = c.Int(),
                        ClassRoom = c.String(),
                    })
                .PrimaryKey(t => t.BuyerId)
                .ForeignKey("dbo.ABuyers", t => t.BuyerId)
                .ForeignKey("dbo.Schools", t => t.school_SchoolId)
                .Index(t => t.BuyerId)
                .Index(t => t.school_SchoolId);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        BuyerId = c.Int(nullable: false),
                        school_SchoolId = c.Int(),
                    })
                .PrimaryKey(t => t.BuyerId)
                .ForeignKey("dbo.ABuyers", t => t.BuyerId)
                .ForeignKey("dbo.Schools", t => t.school_SchoolId)
                .Index(t => t.BuyerId)
                .Index(t => t.school_SchoolId);
            
            CreateTable(
                "dbo.NormalBuyers",
                c => new
                    {
                        BuyerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BuyerId)
                .ForeignKey("dbo.ABuyers", t => t.BuyerId)
                .Index(t => t.BuyerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NormalBuyers", "BuyerId", "dbo.ABuyers");
            DropForeignKey("dbo.Teachers", "school_SchoolId", "dbo.Schools");
            DropForeignKey("dbo.Teachers", "BuyerId", "dbo.ABuyers");
            DropForeignKey("dbo.Students", "school_SchoolId", "dbo.Schools");
            DropForeignKey("dbo.Students", "BuyerId", "dbo.ABuyers");
            DropForeignKey("dbo.Orders", "Customer_BuyerId", "dbo.ABuyers");
            DropForeignKey("dbo.Subjects", "teacher_BuyerId", "dbo.Teachers");
            DropForeignKey("dbo.Orders", "Article_ArticleId", "dbo.AArticles");
            DropForeignKey("dbo.AArticleAPublishers", "APublisher_PublisherId", "dbo.APublishers");
            DropForeignKey("dbo.AArticleAPublishers", "AArticle_ArticleId", "dbo.AArticles");
            DropForeignKey("dbo.Addresses", "AddressId", "dbo.NormalBuyers");
            DropIndex("dbo.NormalBuyers", new[] { "BuyerId" });
            DropIndex("dbo.Teachers", new[] { "school_SchoolId" });
            DropIndex("dbo.Teachers", new[] { "BuyerId" });
            DropIndex("dbo.Students", new[] { "school_SchoolId" });
            DropIndex("dbo.Students", new[] { "BuyerId" });
            DropIndex("dbo.AArticleAPublishers", new[] { "APublisher_PublisherId" });
            DropIndex("dbo.AArticleAPublishers", new[] { "AArticle_ArticleId" });
            DropIndex("dbo.Subjects", new[] { "teacher_BuyerId" });
            DropIndex("dbo.Orders", new[] { "Customer_BuyerId" });
            DropIndex("dbo.Orders", new[] { "Article_ArticleId" });
            DropIndex("dbo.Addresses", new[] { "AddressId" });
            DropTable("dbo.NormalBuyers");
            DropTable("dbo.Teachers");
            DropTable("dbo.Students");
            DropTable("dbo.AArticleAPublishers");
            DropTable("dbo.Subjects");
            DropTable("dbo.Schools");
            DropTable("dbo.APublishers");
            DropTable("dbo.AArticles");
            DropTable("dbo.Orders");
            DropTable("dbo.ABuyers");
            DropTable("dbo.Addresses");
        }
    }
}
