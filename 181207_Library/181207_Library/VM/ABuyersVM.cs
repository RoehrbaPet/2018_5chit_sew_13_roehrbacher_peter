﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181207_Library.VM
{
    class ABuyersVM
    {
        private LibraryContext context;
        public ABuyersVM(LibraryContext context)
        { this.context = context; }


        public BindingList<ABuyer> Orders
        {
            get
            {
                context.Orders.Load();
                return context.Customers.Local.ToBindingList();
            }
        }
    }
}
