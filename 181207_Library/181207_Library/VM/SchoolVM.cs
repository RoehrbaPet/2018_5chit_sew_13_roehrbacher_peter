﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _181207_Library
{
   
        class SchoolVM : INotifyPropertyChanged
        {
            public SchoolVM(School school)
            {
                this.school = school;
                
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected School school;

            public int Schoolid
            {
                get { return school.SchoolId; }
                set
                {
                    school.SchoolId = value;
                    OnPropertyChanged();
                }
            }

            public string Name
            {
                get { return school.Name; }
                set
                {
                    school.Name = value;
                    OnPropertyChanged();
                }
            }

            public BindingList<Teacher> Teachers
            {
                get { return school.Teachers.ToBindingList(); }
                set
                {
                    school.Teachers = value.ToList(); 
                    OnPropertyChanged();
                }
            }

            public BindingList<Student> Students
            {
                get { return school.Students.ToBindingList(); }
                set
                {
                    school.Students = value.ToList();
                    OnPropertyChanged();
                }
            }

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
}
