﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using _181207_Library.Annotations;

namespace _181207_Library.VM
{
    class AddressVM:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected Address address;

        public AddressVM(Address address)
        {
            this.address = address;
        }

        public string ZipCode
        {
            get { return address.ZipCode;}
            set
            {
                address.ZipCode = value;
                OnPropertyChanged();
            }
        }

        public string Street
        {
            get { return address.Street; }
            set
            {
                address.Street = value;
                OnPropertyChanged();
            }
        }

        public string City
        {
            get { return address.City; }
            set
            {
                address.City = value;
                OnPropertyChanged();
            }
        }

        public int AddressId
        {
            get { return address.AddressId; }
            set
            {
                address.AddressId = value; 
                OnPropertyChanged();
            }
        }

        public NormalBuyer CustomerId
        {
            get
            {
                return address.Customer;

            }
            set
            {
                address.Customer = value;
                OnPropertyChanged();
            }
        }
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
