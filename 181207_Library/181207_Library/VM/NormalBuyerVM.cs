﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using _181207_Library.Annotations;

namespace _181207_Library
{
    class NormalBuyerVM:ABuyerVM,INotifyPropertyChanged
    {
        public NormalBuyerVM(NormalBuyer customer)
        {
            base.customer = customer;
        }

        public Address address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged();
            }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
