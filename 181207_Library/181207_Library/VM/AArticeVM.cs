﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _181207_Library
{
    class AArticleVM:INotifyPropertyChanged
    {
        public AArticleVM(AArticle article)
        {
            Article = article;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public AArticle Article
        {
            get { return Article; }
            set
            {
                Article = value;
                OnPropertyChanged();
            }
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
