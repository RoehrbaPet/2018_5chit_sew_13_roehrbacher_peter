﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using _181207_Library.Annotations;

namespace _181207_Library
{
    class TeacherVM:ABuyerVM, INotifyPropertyChanged
    {
        public TeacherVM(Teacher teacher)
        {
            base.customer = teacher;
        }
        //TODO Change to SchoolVM
        public School school
        {
            get
            {
                Teacher t = (Teacher) customer;
                return t.school;
            }
            set
            {
                Teacher t = (Teacher)customer;
               t.school = value;
                OnPropertyChanged();
            }
        }

        public BindingList<Subject> Subjects
        {
            get { return ((Teacher) base.customer).Subjects.ToBindingList(); }
            set
            {
                ((Teacher) base.customer).Subjects = value.ToList();
                OnPropertyChanged();
            }
        }
        

       
    }

    public static class TExtensions
    {
        public static List<T> ToList<T>(this IEnumerable<T> collection)
        {
            return new List<T>(collection);
        }

        public static BindingList<T> ToBindingList<T>(this IEnumerable<T> collection)
        {
            return new BindingList<T>(collection.ToList());
        }
    }
}
