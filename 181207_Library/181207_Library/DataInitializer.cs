﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace _181207_Library
{
    class DataInitializer:DropCreateDatabaseAlways<LibraryContext>
    {
        public void Seed(LibraryContext context)
        {

            var magazines = new List<AArticle>
            {
                new Magazine {Title = "Most successful actors of all time", Category="Celebrities"},
                new Magazine {Title = "How solve the plastic problem", Category="Environment"},
                new Magazine {Title = "Lions ... Kings of Africa", Category="Animals"}
            };


            var books = new List<AArticle>
            {
                new Book {Title = "Steve Jobs ... Legend", Category="Biographies", Isbn="834-1-1212-8"},
                new Book {Title = "How technology changed everyones life", Category="Technology", Isbn="456-3-78978-4"},
                new Book {Title = "Costa Rica ... a beautiful place", Category="Nature", Isbn="234-1-23423-5"}
            };


            var authors = new List<APublisher>()
            {
                new Author { Name="Steve Black", Articles = new List<AArticle>(){ magazines[0] } },
                new Author { Name="Tony Stark", Articles = new List<AArticle>(){ books[0] } },
                new Author { Name="Mike Wally", Articles = new List<AArticle>(){ magazines[1] } }
            };


            var publishers = new List<APublisher>
            {
                new Publisher { Name="Forbes", Articles = new List<AArticle>(){ books[1], magazines[2] } },
                new Publisher { Name="Gadget", Articles = new List<AArticle>(){ magazines[2] } },
                new Publisher { Name="American Photo", Articles = new List<AArticle>(){ books[2] } }
            };


            var addresses = new List<Address>
            {
                new Address { City = "New York", Street = "State Str", ZipCode = "234234" },
                new Address { City = "New York", Street = "Martin Luther King Str", ZipCode = "234423" },
                new Address { City = "St. Louis", Street = "Forest Str", ZipCode = "354354" }
            };

            var schools = new List<School>
            {
                new School { Name="Cristo Rey New York High School" },
                new School { Name="Stuyvesant High School" },
                new School { Name="Abraham Lincoln High School" }
            };

            var students = new List<Student>
            {
                new Student { FirstName = "Tony", LastName = "Graham", ClassRoom = "9A", school = schools[0] },
                new Student { FirstName = "Marcus", LastName = "Graham", ClassRoom = "9A", school = schools[0] },
                new Student { FirstName = "Ben", LastName = "Stiller", ClassRoom = "10A", school = schools[1] },
                new Student { FirstName = "Ken", LastName = "Todd", ClassRoom = "12B", school = schools[1] },
                new Student { FirstName = "Fill", LastName = "Miller", ClassRoom = "12C", school = schools[2] },
                new Student { FirstName = "Michael", LastName = "Star", ClassRoom = "10C", school = schools[2] }
            };

            var teachers = new List<Teacher>
            {
                new Teacher { FirstName = "Theresa", LastName = "Miller", school = schools[0]},
                new Teacher { FirstName = "Thomas", LastName = "Smith", school = schools[0] },
                new Teacher { FirstName = "Bianca", LastName = "Stone", school = schools[1] },
                new Teacher { FirstName = "Benjamin", LastName = "Gates", school = schools[1] },
                new Teacher { FirstName = "Walter", LastName = "White", school = schools[2] },
                new Teacher { FirstName = "Louise", LastName = "Smith", school = schools[2] }
            };

            var subjects = new List<Subject>
            {
                new Subject { Name="Physics", teacher=teachers[0] },
                new Subject { Name="Sports", teacher=teachers[2] },
                new Subject { Name="Maths", teacher=teachers[4] },
                new Subject { Name="Chemics", teacher=teachers[0] },
                new Subject { Name="Biology", teacher=teachers[1] },
                new Subject { Name="Geography", teacher=teachers[1] },
                new Subject { Name="History", teacher=teachers[5] }
            };

            var normalBuyer = new List<NormalBuyer>
            {
                new NormalBuyer { FirstName = "Simon", LastName = "Mosby", address = addresses[0] },
                new NormalBuyer { FirstName = "Barney", LastName = "Stinson", address = addresses[2] }
            };

            var orders = new List<Order>
            {
                new Order { Date=DateTime.Parse("2018-07-04"), Customer = normalBuyer[0], Article = magazines[0] },
                new Order { Date=DateTime.Parse("2018-07-04"), Customer = normalBuyer[0], Article = magazines[1] },
                new Order { Date=DateTime.Parse("2018-07-04"), Customer = normalBuyer[0], Article = books[0] },
                new Order { Date=DateTime.Parse("2018-08-13"), Customer = normalBuyer[1], Article = magazines[0] },
                new Order { Date=DateTime.Parse("2018-08-13"), Customer = normalBuyer[1], Article = magazines[1] },
                new Order { Date=DateTime.Parse("2018-08-13"), Customer = normalBuyer[1], Article = books[2] },
                new Order { Date=DateTime.Parse("2017-06-15"), Customer = teachers[2], Article = magazines[0] },
                new Order { Date=DateTime.Parse("2016-07-04"), Customer = teachers[2], Article = books[0] },
                new Order { Date=DateTime.Parse("2017-06-15"), Customer = teachers[3], Article = magazines[1] },
                new Order { Date=DateTime.Parse("2016-07-04"), Customer = teachers[3], Article = magazines[2] },
                new Order { Date=DateTime.Parse("2017-06-15"), Customer = teachers[4], Article = books[0] },
                new Order { Date=DateTime.Parse("2016-07-04"), Customer = teachers[4], Article = books[1] },
                new Order { Date=DateTime.Parse("2017-10-09"), Customer = students[3], Article = magazines[0] },
                new Order { Date=DateTime.Parse("2018-05-24"), Customer = students[3], Article = books[1] },
                new Order { Date=DateTime.Parse("2017-10-09"), Customer = students[2], Article = magazines[0] },
                new Order { Date=DateTime.Parse("2018-05-24"), Customer = students[1], Article = books[1] },
                new Order { Date=DateTime.Parse("2017-10-09"), Customer = students[0], Article = magazines[2] },
                new Order { Date=DateTime.Parse("2018-05-24"), Customer = students[5], Article = books[0] }
            };

            magazines.ForEach(x => context.Articles.Add(x));
            books.ForEach(x => context.Articles.Add(x));
            authors.ForEach(x => context.Publishers.Add(x));
            publishers.ForEach(x => context.Publishers.Add(x));
            addresses.ForEach(x => context.Addresses.Add(x));
            students.ForEach(x => context.Customers.Add(x));
            subjects.ForEach(x => context.Subjects.Add(x));
            teachers.ForEach(x => context.Customers.Add(x));
            normalBuyer.ForEach(x => context.Customers.Add(x));
            schools.ForEach(x => context.Schools.Add(x));
            orders.ForEach(x => context.Orders.Add(x));

            context.SaveChanges();
            base.Seed(context);
        }


    }
}
