﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace _181207_Library
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("Library")
        {
            Database.SetInitializer<LibraryContext>(new DataInitializer());
            
                //Database.Initialize(true);
               
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<APublisher>()
                .Map<Publisher>(x => x.Requires("PublisherType").HasValue("Publisher"))
                .Map<Author>(x => x.Requires("PublisherType").HasValue("Author"));
            modelBuilder.Entity<AArticle>()
                .Map<Magazine>(x => x.Requires("ArticleType").HasValue("Magazine"))
                .Map<Book>(x => x.Requires("ArticleType").HasValue("Book"));
            modelBuilder.Entity<Student>().ToTable("Students");
            modelBuilder.Entity<Teacher>().ToTable("Teachers");
            modelBuilder.Entity<NormalBuyer>().ToTable("NormalBuyers");
            modelBuilder.Entity<NormalBuyer>().HasRequired(x => x.address)
                .WithRequiredPrincipal(x => x.Customer);
            modelBuilder.Entity<Teacher>().HasOptional(x => x.school);
            modelBuilder.Entity<Student>().HasOptional(x => x.school);
            modelBuilder.Entity<Subject>().HasOptional(x => x.teacher);
            modelBuilder.Entity<School>().HasMany(x => x.Teachers);
            modelBuilder.Entity<School>().HasMany(x => x.Students);
            modelBuilder.Entity<Teacher>().HasMany(x => x.Subjects);
            modelBuilder.Entity<ABuyer>().HasMany(x => x.Orders);
            modelBuilder.Entity<Order>().HasOptional(x => x.Customer);
            modelBuilder.Entity<APublisher>().HasMany(x => x.Articles);
            modelBuilder.Entity<AArticle>().HasMany(x => x.Publishers);

        }

        public DbSet<APublisher> Publishers { get; set; }
        public DbSet<AArticle> Articles { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ABuyer> Customers { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Address> Addresses { get; set; }
    }


    public abstract class APublisher
    {
        [Key]
        public int PublisherId { get; set; }
        public string Name { get; set; }

        public List<AArticle> Articles { get; set; }
    }

    public abstract class AArticle
    {
        [Key]
        public int ArticleId { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }

        public List<APublisher> Publishers { get; set; }
    }

    public class Author : APublisher
    {
    }

    public class Publisher : APublisher
    {
    }

    public class Magazine : AArticle
    {
    }

    public class Book : AArticle
    {
        public string Isbn { get; set; }
    }

    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        public DateTime Date { get; set; }

        public ABuyer Customer { get; set; }
        public AArticle Article { get; set; }
    }

    public abstract class ABuyer
    {
        [Key]
        public int BuyerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<Order> Orders { get; set; }
    }

    public class Student : ABuyer
    {
        public string ClassRoom { get; set; }
        public School school { get; set; }
    }

    public class School
    {
        [Key]
        public int SchoolId { get; set; }
        public string Name { get; set; }

        public List<Student> Students { get; set; }
        public List<Teacher> Teachers { get; set; }
    }

    public class Teacher : ABuyer
    {
        public School school { get; set; }
        public List<Subject> Subjects { get; set; }
    }

    public class Subject
    {
        [Key]
        public int SubjectId { get; set; }
        public string Name { get; set; }

        public Teacher teacher { get; set; }
    }

    public class NormalBuyer : ABuyer
    {
        public Address address { get; set; }
    }

    public class Address
    {
        [Key]
        public int AddressId { get; set; }
      
        public string City { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }

        public NormalBuyer Customer { get; set; }
    }
}