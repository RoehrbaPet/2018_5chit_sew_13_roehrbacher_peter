﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablePerConcreteClass
{
    class Program
    {
        static void Main(string[] args)
        {
            TablePerConcreteClassContext tphc = new TablePerConcreteClassContext();
            tphc.Staff.Add(new Teacher() { Id = 3, Name = "Yeees", StaffId = 3 });
            tphc.SaveChanges();
        }
    }

    class TablePerConcreteClassContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Students");
            });

            modelBuilder.Entity<Teacher>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Teachers");
            });
        }

        public TablePerConcreteClassContext() : base("TablePerConcreteClass")
        {

        }
        public DbSet<Person> Staff { get; set; }
    }

    public abstract class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Student : Person
    {
        public int StudentId { get; set; }
    }

    public class Teacher : Person
    {
        public int StaffId { get; set; }
    }
    public class StudentAddress
    {
        public int StudentAddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int Zipcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public virtual Student Student { get; set; }
    }
}
