﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablePerType
{
    class Program
    {
        static void Main(string[] args)
        {
            TablePerTypeContext tphc = new TablePerTypeContext();
            tphc.Staff.Add(new Teacher() { Id = 3, Name = "Yeees", StaffId = 3 });
            tphc.SaveChanges();
        }
    }

    class TablePerTypeContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
                .HasMany(x => x.Courses)
                .WithMany(c => c.Students)
                .Map(cs =>
                {
                    cs.MapLeftKey("StudentRefId");
                    cs.MapRightKey("CourseRefId");
                    cs.ToTable("Student_Course");
                });

        }



        public TablePerTypeContext() : base("TablePerType")
        {
            
        }    


    public DbSet<Person> Staff { get; set; }

    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    [Table("Students")]
    public class Student : Person
    {
        public Student()
        {
            this.Courses = new HashSet<Course>();
        }

        public int StudentId { get; set; }
        [Required]
        

        public virtual ICollection<Course> Courses { get; set; }
    }

    [Table("Teachers")]
    public class Teacher : Person
    {
        public int StaffId { get; set; }
    }

    public class Course
    {
        public Course()
        {
            this.Students = new HashSet<Student>();
        }

        public int CourseId { get; set; }
        public string CourseName { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}
