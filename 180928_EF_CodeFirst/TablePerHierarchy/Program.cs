﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablePerHierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            TablePerHierarchyContext tphc = new TablePerHierarchyContext();
            tphc.Configuration.AutoDetectChangesEnabled = true;
            tphc.Staff.Add(new Teacher() {Id = 5, Name = "yes", StaffId = 5});
            Student t = new Student(){Id = 8, Name = "haha", StudentId = 8}
            ;
            tphc.Grades.Add(new Grade(){GradeId = 5, GradeName = "First", Section = "two?", Students = new List<Student>(new Student[] {t})});
            tphc.SaveChanges();
        }
    }

    class TablePerHierarchyContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
               
            
            modelBuilder.Entity<Person>()
                .HasIndex(t => t.Id);
        }
    

        public TablePerHierarchyContext() : base("TablePerHierarchy")
        {
            
        }
        public DbSet<Person> Staff { get; set; }
        public DbSet<Grade> Grades { get; set; }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Student : Person
    {
        public int StudentId { get; set; }
        public Grade CurrentGrade { get; set; }
    }

    public class Teacher : Person
    {
        public int StaffId { get; set; }
    }
    public class Grade
    {
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public string Section { get; set; }

        public ICollection<Student> Students { get; set; }

    }
}
