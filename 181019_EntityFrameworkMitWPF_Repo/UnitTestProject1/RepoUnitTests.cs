﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using _181019_EntityFrameworkMitWPF_Repo.Controller;
using _181019_EntityFrameworkMitWPF_Repo.Model;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class RepoUnitTests
    {
        private CarRepository carRepository = new CarRepository(new CarContext());
        private CreditRiskRepository creditRiskRepository = new CreditRiskRepository(new CarContext());
        private CustomerRepository customerRepository = new CustomerRepository(new CarContext());

        [TestMethod]
        public void GetCarsByColor()
        {
            List<Car> byColor = carRepository.GetCarsByColor("Black");
            Assert.AreEqual(4, byColor.Count);
        }

        [TestMethod]
        public void CountCarsByColor()
        {
            int cnt = carRepository.CountCarsByColor("Black");
            Assert.AreEqual(4, cnt);
        }

        [TestMethod]
        public void GetCarsOrderedByMake()
        {
            List<Car> byMake = carRepository.GetCarsOrderedByMake();
            Assert.AreEqual(5, byMake[0].CarId);
            Assert.AreEqual(6, byMake[1].CarId);
        }

        [TestMethod]
        public void GetCarsByMakeAndColor()
        {
            List<Car> byMakeAndColor = carRepository.GetAll().GetCarsByMakeAndColor("BMW", "Pink");
            Assert.AreEqual(1, byMakeAndColor.Count);
            Assert.AreEqual(7, byMakeAndColor[0].CarId);
        }

        [TestMethod]
        public void GetCarByMakeOrderedByColor()
        {
            List<Car> byMake = carRepository.GetAll().GetCarByMakeOrderedByColor("Yugo");
            Assert.AreEqual(9, byMake[0].CarId);
            Assert.AreEqual(4, byMake[1].CarId);
        }

        [TestMethod]
        public void GetCreditRisksByLastName()
        {
            List<CreditRisk> byLastName = creditRiskRepository.GetCreditRisksByLastName("Customer");
            Assert.AreEqual(1, byLastName.Count);

            byLastName = creditRiskRepository.GetCreditRisksByLastName("Non existent");
            Assert.AreEqual(0, byLastName.Count);
        }

        [TestMethod]
        public void CountCreditRisksByLastName()
        {
            int cnt = creditRiskRepository.CountCreditRisksByLastName("Customer");
            Assert.AreEqual(1, cnt);

            cnt = creditRiskRepository.CountCreditRisksByLastName("Non existent");
            Assert.AreEqual(0, cnt);
        }

        [TestMethod]
        public void GetCreditRisksOrderedByLastName()
        {
            List<CreditRisk> sorted = creditRiskRepository.GetCreditRisksOrderedByLastName();
            Assert.AreEqual(1, sorted[0].CustId);
        }

        [TestMethod]
        public void GetCustomersByLastName()
        {
            List<Customer> byLastName = customerRepository.GetCustomersByLastName("Walton");
            Assert.AreEqual(2, byLastName.Count);
        }

        [TestMethod]
        public void CountCustomersByLastName()
        {
            int cnt = customerRepository.CountCustomersByLastName("Walton");
            Assert.AreEqual(2, cnt);
        }

        [TestMethod]
        public void GetCustomersOrderedByLastName()
        {
            List<Customer> sorted = customerRepository.GetCustomersOrderedByLastName();
            Assert.AreEqual(1, sorted[0].CustId);
            Assert.AreEqual(5, sorted[1].CustId);
        }
    }
}
