﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.Controller
{
    public static class CarExtensions
    {
        public static List<Car> GetCarsByMakeAndColor(this List<Car> cars, string make, string color)
            => (from car in cars
                where car.Make == make && car.Color == color
                select car).ToList();

        public static List<Car> GetCarByMakeOrderedByColor(this List<Car> cars, string make)
            => (from car in cars
                where car.Make == make
                orderby car.Color
                select car).ToList();
    }
}
