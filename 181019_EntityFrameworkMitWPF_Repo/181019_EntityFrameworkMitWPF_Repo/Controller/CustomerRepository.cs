﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.Controller
{
    public class CustomerRepository : ARepository<Customer, int>
    {
        protected override DbSet<Customer> Table { get => Context.Customers; }

        public CustomerRepository(CarContext context) : base(context) { }

        public List<Customer> GetCustomersByLastName(string lastName) =>
            (from customer in Table
             where customer.LastName == lastName
             select customer).ToList();

        public int CountCustomersByLastName(string lastName) =>
            (from customer in Table
             where customer.LastName == lastName
             select customer).Count();

        public List<Customer> GetCustomersOrderedByLastName() =>
            (from customer in Table
             orderby customer.LastName
             select customer).ToList();
    }
}
