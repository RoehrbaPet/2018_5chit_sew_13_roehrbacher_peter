﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.Controller
{
    public class CreditRiskRepository : ARepository<CreditRisk, int>
    {
        protected override DbSet<CreditRisk> Table { get => Context.CreditRisks; }

        public CreditRiskRepository(CarContext context) : base(context) { }

        public List<CreditRisk> GetCreditRisksByLastName(string lastName) =>
            (from creditRisk in Table
             where creditRisk.LastName == lastName
             select creditRisk).ToList();

        public int CountCreditRisksByLastName(string lastName) =>
            (from creditRisk in Table
             where creditRisk.LastName == lastName
             select creditRisk).Count();

        public List<CreditRisk> GetCreditRisksOrderedByLastName() =>
            (from creditRisk in Table
             orderby creditRisk.LastName
             select creditRisk).ToList();
    }
}
