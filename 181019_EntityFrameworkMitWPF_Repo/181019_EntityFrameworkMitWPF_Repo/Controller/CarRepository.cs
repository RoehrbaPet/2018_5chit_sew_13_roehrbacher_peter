﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.Controller
{
    public class CarRepository : ARepository<Car, int>
    {
        protected override DbSet<Car> Table { get => Context.Cars; }

        public CarRepository(CarContext context) : base(context) { }

        public List<Car> GetCarsByColor(string color) =>
            (from car in Table
            where car.Color == color
            select car).ToList();

        public int CountCarsByColor(string color) =>
            (from car in Table
             where car.Color == color
             select car).Count();

        public List<Car> GetCarsOrderedByMake() =>
            (from car in Table
             orderby car.Make
             select car).ToList();
    }
}
