﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.Controller
{
    public abstract class ARepository<T, TId> : IDisposable
        where T : class, new()
    {
        protected CarContext Context { get; }
        protected abstract DbSet<T> Table { get; }

        public ARepository(CarContext context)
        {
            this.Context = context;
        }

        public List<T> GetAll() => Table.ToList();
        public T GetOne(TId id) => Table.Find(id);
        
        public int Add(T entity) { Table.Add(entity); return Context.SaveChanges(); }
        public int AddRange(IEnumerable<T> entities) { Table.AddRange(entities); return Context.SaveChanges(); }
        public int Delete(T entity) { Table.Remove(entity); return Context.SaveChanges(); }
        public int DeleteRange(IEnumerable<T> entities) { Table.RemoveRange(entities); return Context.SaveChanges(); }

        public int Update() => Context.SaveChanges();

        public void Dispose() => Context.Dispose();
    }
}