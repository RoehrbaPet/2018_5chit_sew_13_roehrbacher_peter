﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.View
{
    class CarVM : AViewModel
    {
        private Car car;

        public int CarId
        {
            get => car.CarId;
            set { car.CarId = value; CallPropertyChanged("CarId"); }
        }
        public string Make
        {
            get => car.Make;
            set { car.Make = value; CallPropertyChanged("Make"); }
        }
        public string Color
        {
            get => car.Color;
            set { car.Color = value; CallPropertyChanged("Color"); }
        }
        public string CarNickName
        {
            get => car.CarNickName;
            set { car.CarNickName = value; CallPropertyChanged("CarNickName"); }
        }

        public CarVM(Car car)
        {
            this.car = car;
        }
    }
}
