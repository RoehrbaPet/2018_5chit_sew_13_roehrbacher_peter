﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.View
{
    class CustomerVM : AViewModel
    {
        private Customer customer;

        public int CustId
        {
            get => customer.CustId;
            set { customer.CustId = value; CallPropertyChanged("CustId"); }
        }
        public string FirstName
        {
            get => customer.FirstName;
            set { customer.FirstName = value; CallPropertyChanged("FirstName"); CallPropertyChanged("FullName"); }
        }
        public string LastName
        {
            get => customer.LastName;
            set { customer.LastName = value; CallPropertyChanged("LastName"); CallPropertyChanged("FullName"); }
        }
        public string FullName
        {
            get => customer.FullName;
        }

        public CustomerVM(Customer customer)
        {
            this.customer = customer;
        }
    }
}
