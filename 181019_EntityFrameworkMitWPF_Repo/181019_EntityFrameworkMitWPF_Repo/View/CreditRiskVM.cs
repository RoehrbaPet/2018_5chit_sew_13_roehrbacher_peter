﻿using _181019_EntityFrameworkMitWPF_Repo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.View
{
    class CreditRiskVM : AViewModel
    {
        private CreditRisk creditRisk;

        public int CustId
        {
            get => creditRisk.CustId;
            set { creditRisk.CustId = value; CallPropertyChanged("CustId"); }
        }
        public string FirstName
        {
            get => creditRisk.FirstName;
            set { creditRisk.FirstName = value; CallPropertyChanged("FirstName"); }
        }
        public string LastName
        {
            get => creditRisk.LastName;
            set { creditRisk.LastName = value; CallPropertyChanged("LastName"); }
        }

        public CreditRiskVM(CreditRisk creditRisk)
        {
            this.creditRisk = creditRisk;
        }
    }
}
