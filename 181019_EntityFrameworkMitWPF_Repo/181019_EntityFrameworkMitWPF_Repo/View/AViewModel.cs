﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.View
{
    abstract class AViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void CallPropertyChanged(string property) =>
            PropertyChanged(this, new PropertyChangedEventArgs(property));
    }
}
