﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181019_EntityFrameworkMitWPF_Repo.View
{
    static class ViewModelConverter
    {
        public static List<TViewModel> ConvertToViewModelList<TEntity, TViewModel>(this IEnumerable<TEntity> entities, Func<TEntity, TViewModel> instantiate)
            where TViewModel : AViewModel =>
            (from cur in entities
             select instantiate(cur)).ToList();
    }
}
