﻿using _181019_EntityFrameworkMitWPF_Repo.Controller;
using _181019_EntityFrameworkMitWPF_Repo.Model;
using _181019_EntityFrameworkMitWPF_Repo.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _181019_EntityFrameworkMitWPF_Repo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CarRepository carRepo;
        private CustomerRepository customerRepo;
        private CreditRiskRepository creditRiskRepo;

        public MainWindow()
        {
            InitializeComponent();

            CarContext context = new CarContext();
            carRepo = new CarRepository(context);
            customerRepo = new CustomerRepository(context);
            creditRiskRepo = new CreditRiskRepository(context);

            RefreshCars();
            RefreshCustomers();
            RefreshCreditRisks();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            carRepo.Update();
            customerRepo.Update();
            creditRiskRepo.Update();
        }

        private void RefreshCars()
        {
            dgrCars.ItemsSource = carRepo.GetAll().ConvertToViewModelList(car => new CarVM(car));
            dgrCarsSorted.ItemsSource = carRepo.GetCarsOrderedByMake().ConvertToViewModelList(car => new CarVM(car));
            dgrCarsSearchColor.ItemsSource = carRepo.GetCarsByColor(txtCarSearchColor.Text).ConvertToViewModelList(car => new CarVM(car));
            lblCarSearchColor.Content = carRepo.CountCarsByColor(txtCarSearchColor.Text).ToString();
        }
        private void txtCarColor_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgrCarsSearchColor.ItemsSource = carRepo.GetCarsByColor(txtCarSearchColor.Text).ConvertToViewModelList(car => new CarVM(car));
            lblCarSearchColor.Content = carRepo.CountCarsByColor(txtCarSearchColor.Text).ToString();
        }
        private void btnCarAdd_Click(object sender, RoutedEventArgs e)
        {
            carRepo.Add(new Car() { Make = txtCarAddMake.Text, Color = txtCarAddColor.Text, CarNickName = txtCarAddNickName.Text });
            RefreshCars();

            txtCarAddMake.Text = "";
            txtCarAddColor.Text = "";
            txtCarAddNickName.Text = "";
        }
        private void btnCarDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                carRepo.Delete(carRepo.GetOne(Int32.Parse(txtCarDeleteId.Text)));
                RefreshCars();

                txtCarDeleteId.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Meh.", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void dgrCars_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            carRepo.Update();
        }

        private void RefreshCustomers()
        {
            dgrCustomers.ItemsSource = customerRepo.GetAll().ConvertToViewModelList(customer => new CustomerVM(customer));
            dgrCustomersSorted.ItemsSource = customerRepo.GetCustomersOrderedByLastName().ConvertToViewModelList(customer => new CustomerVM(customer));
            dgrCustomersSearchLastName.ItemsSource = customerRepo.GetCustomersByLastName(txtCustomersSearchLastName.Text).ConvertToViewModelList(customer => new CustomerVM(customer));
            lblCustomersSearchLastName.Content = customerRepo.CountCustomersByLastName(txtCustomersSearchLastName.Text).ToString();
        }
        private void txtCustomersSearchLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgrCustomersSearchLastName.ItemsSource = customerRepo.GetCustomersByLastName(txtCustomersSearchLastName.Text).ConvertToViewModelList(customer => new CustomerVM(customer));
            lblCustomersSearchLastName.Content = customerRepo.CountCustomersByLastName(txtCustomersSearchLastName.Text).ToString();
        }
        private void btnCustomerAdd_Click(object sender, RoutedEventArgs e)
        {
            customerRepo.Add(new Customer() { FirstName = txtCustomersAddFirstName.Text, LastName = txtCustomersAddLastName.Text });
            RefreshCustomers();

            txtCustomersAddFirstName.Text = "";
            txtCustomersAddLastName.Text = "";
        }
        private void btnCustomerDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                customerRepo.Delete(customerRepo.GetOne(Int32.Parse(txtCustomersDeleteId.Text)));
                RefreshCustomers();

                txtCustomersDeleteId.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Meh.", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void dgrCustomers_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            customerRepo.Update();
        }


        private void RefreshCreditRisks()
        {
            dgrCreditRisks.ItemsSource = creditRiskRepo.GetAll().ConvertToViewModelList(creditRisk => new CreditRiskVM(creditRisk));
            dgrCreditRisksSorted.ItemsSource = creditRiskRepo.GetCreditRisksOrderedByLastName().ConvertToViewModelList(creditRisk => new CreditRiskVM(creditRisk));
            dgrCreditRisksSearchLastName.ItemsSource = creditRiskRepo.GetCreditRisksByLastName(txtCreditRisksSearchLastName.Text).ConvertToViewModelList(creditRisk => new CreditRiskVM(creditRisk));
            lblCreditRisksSearchLastName.Content = creditRiskRepo.CountCreditRisksByLastName(txtCreditRisksSearchLastName.Text).ToString();
        }
        private void txtCreditRisksSearchLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgrCreditRisksSearchLastName.ItemsSource = creditRiskRepo.GetCreditRisksByLastName(txtCreditRisksSearchLastName.Text).ConvertToViewModelList(creditRisk => new CreditRiskVM(creditRisk));
            lblCreditRisksSearchLastName.Content = creditRiskRepo.CountCreditRisksByLastName(txtCreditRisksSearchLastName.Text).ToString();
        }
        private void btnCreditRisksAdd_Click(object sender, RoutedEventArgs e)
        {
            creditRiskRepo.Add(new CreditRisk() { FirstName = txtCreditRisksAddFirstName.Text, LastName = txtCreditRisksAddLastName.Text });
            RefreshCreditRisks();

            txtCreditRisksAddFirstName.Text = "";
            txtCreditRisksAddLastName.Text = "";
        }
        private void btnCreditRisksDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                creditRiskRepo.Delete(creditRiskRepo.GetOne(Int32.Parse(txtCreditRisksDeleteId.Text)));
                RefreshCreditRisks();

                txtCreditRisksDeleteId.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Meh.", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void dgrCreditRisks_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            creditRiskRepo.Update();
        }
    }
}
