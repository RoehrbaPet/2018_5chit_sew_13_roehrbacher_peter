using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181019_EntityFrameworkMitWPF_Repo.Model

{ 
	public partial class Car
	{
		public override string ToString()
		{
			// Since the PetName column could be empty, supply
			// the default name of **No Name**.
			return $"{this.CarNickName ?? "**No Name**"} is a" +
                $" {this.Color} {this.Make} with ID {this.CarId}.";
		}

        [NotMapped]
	    public string MakeColor => $"{Make} + ({Color})";

        [StringLength(50), Column("PetName")]
        public string CarNickName { get; set; }
	}
}