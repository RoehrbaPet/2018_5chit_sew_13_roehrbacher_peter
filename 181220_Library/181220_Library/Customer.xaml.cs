﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using _181220_Library.VM;

namespace _181220_Library
{
    /// <summary>
    /// Interaction logic for Customer.xaml
    /// </summary>
    public partial class Customer : Window
    {
        private ABuyer customer;
        public Customer(ABuyer Customer)
        {
            InitializeComponent();
            this.customer = Customer;
            GenerateUI();
        }

        private void GenerateUI()
        {
            if (customer != null)
            {
                txt_FirstName.Text = customer.FirstName;
                txt_LastName.Text = customer.LastName;

                if (!(customer is Student))
                {
                    txt_ClassRoom.IsEnabled = false;
                }
                else
                {
                    txt_ClassRoom.IsEnabled = true;
                    txt_ClassRoom.Text = ((Student) customer).ClassRoom;
                }

                if (customer is Teacher || customer is Student)
                {
                     cmb_Combo.ItemsSource = new SchoolsVM(LibraryContext.Library).Publishers;
                    cmb_Combo.DisplayMemberPath = "Name";
                    
                    if (customer is Teacher)
                    {
                        rdb_Teacher.IsChecked = true;
                        cmb_Combo.SelectedItem = ((Teacher) customer).school;
                    }
                    else
                    {
                        rdb_Student.IsChecked = true;
                        cmb_Combo.SelectedItem = ((Student)customer).school;

                    }
                }
                else
                {
                    cmb_Combo.ItemsSource = new AAddressesVM(LibraryContext.Library).Publishers;
                    cmb_Combo.SelectedItem = ((NormalBuyer)customer).address;
                    rdb_NormalBuyer.IsChecked = true;
                }

                rdb_NormalBuyer.IsEnabled = false;
                rdb_Student.IsEnabled = false;
                rdb_Teacher.IsEnabled = false;

            }
            else
            {
                txt_ClassRoom.IsEnabled = false;
                cmb_Combo.IsEnabled = false;
            }
        }

        private void rdb_NormalBuyer_Checked(object sender, RoutedEventArgs e)
        {
            cmb_Combo.IsEnabled = true;

            cmb_Combo.ItemsSource = new AAddressesVM(LibraryContext.Library).Publishers;
            cmb_Combo.DisplayMemberPath = "";
            txt_ClassRoom.IsEnabled = false;
            customer = new NormalBuyer();
        }

        private void rdb_Teacher_Checked(object sender, RoutedEventArgs e)
        {
            cmb_Combo.IsEnabled = true;

            cmb_Combo.ItemsSource = new SchoolsVM(LibraryContext.Library).Publishers;
            cmb_Combo.DisplayMemberPath = "Name";
            txt_ClassRoom.IsEnabled = false;
            customer = new Teacher();
        }

        private void rdb_Student_Checked(object sender, RoutedEventArgs e)
        {
            txt_ClassRoom.IsEnabled = true;
            cmb_Combo.IsEnabled = true;
            cmb_Combo.ItemsSource = new SchoolsVM(LibraryContext.Library).Publishers;
            cmb_Combo.DisplayMemberPath = "Name";
            customer = new Student();
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            customer.FirstName = txt_FirstName.Text;
            customer.LastName = txt_LastName.Text;

            if (customer is Teacher)
            {
                ((Teacher) customer).school = LibraryContext.Library.Schools.Find(((SchoolVM)cmb_Combo.SelectedItem).Schoolid);
            }

            if (customer is Student)
            {
                ((Student)customer).school = LibraryContext.Library.Schools.Find(((SchoolVM)cmb_Combo.SelectedItem).Schoolid);
                ((Student) customer).ClassRoom = txt_ClassRoom.Text;
            }

            if (customer is NormalBuyer)
            {
                ((NormalBuyer)customer).address = LibraryContext.Library.Addresses.Find(((AddressVM)cmb_Combo.SelectedItem).AddressId);
            }

            if (LibraryContext.Library.Customers.Find(customer.BuyerId) == null)
            {
                LibraryContext.Library.Customers.Add(customer);
            }
            else
            {
                LibraryContext.Library.Customers.Attach(customer);
            }
            LibraryContext.Library.SaveChanges();
            this.Close();
        }
    }
}
