﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _181220_Library.Command;
using _181220_Library.VM;

namespace _181220_Library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public BindingList<OrderVM> Orders;
        public BindingList<ABuyerVM> Customers;
        public BindingList<APublisherVM> Publishers;
        public AddCustomer AddCommand => new AddCustomer(Add);
        public void Add()
        {
            Customer c = new Customer(null);
            c.Show();
        }
        public MainWindow()
        {
            InitializeComponent();
            LibraryContext lc = LibraryContext.Library;
            lc.Orders.Load();
            MessageBox.Show(lc.Orders.Local.Count.ToString());
            lc.SaveChanges();
            Orders = new OrdersVM(lc).Orders;
            Customers = new ABuyersVM(lc).ABuyers;
            Publishers = new APublishersVM(lc).Publishers;

            dtg_Orders.ItemsSource = Orders;
            dtg_Customers.ItemsSource = Customers;
            dtg_Publishers.ItemsSource = Publishers;
            btn_DelCustomer.DataContext = dtg_Customers.SelectedItem;
            btn_AddCustomer.DataContext = this;
        }

       


        private void dtg_Customers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btn_EditCustomer.DataContext = dtg_Customers.SelectedItem;
            btn_DelCustomer.DataContext = dtg_Customers.SelectedItem;
        }
    }
}
