﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _181220_Library.Command
{
    public class AddCustomer:ICommand
    {
        private Action ex;

        public AddCustomer(Action ex)
        {
            this.ex = ex;
        }
        public bool CanExecute(object parameter)
        {
            return ex != null;
        }

        public void Execute(object parameter)
        {
            ex();
        }

        public event EventHandler CanExecuteChanged;
    }
}
