﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181220_Library.VM
{
    class SchoolsVM
    {
        private LibraryContext context;
        public SchoolsVM(LibraryContext context)
        { this.context = context; }


        public BindingList<SchoolVM> Publishers
        {
            get
            {              
                context.Schools.Load();
                BindingList<SchoolVM> schools = new BindingList<SchoolVM>();
                context.Schools.Local.ToList().ForEach(x => schools.Add(new SchoolVM(x)));
                return schools;

            }
        }
    }
}
