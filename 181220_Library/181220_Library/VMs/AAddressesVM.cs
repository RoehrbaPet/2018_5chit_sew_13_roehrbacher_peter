﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181220_Library.VM
{
    class AAddressesVM
    {
        private LibraryContext context;
        public AAddressesVM(LibraryContext context)
        { this.context = context; }


        public BindingList<AddressVM> Publishers
        {
            get
            {
                context.Customers.Load();
                context.Addresses.Load();
                BindingList<AddressVM> addresses = new BindingList<AddressVM>();
                context.Addresses.Local.ToList().ForEach(x => addresses.Add(new AddressVM(x)));
                return addresses;

            }
        }
    }
}
