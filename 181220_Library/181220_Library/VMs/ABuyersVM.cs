﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _181220_Library.Command;

namespace _181220_Library.VM
{
    class ABuyersVM
    {
        private LibraryContext context;
        public ABuyersVM(LibraryContext context)
        { this.context = context; }

        public BindingList<ABuyerVM> ABuyers
        {
            get
            {
                context.Customers.Load();
                BindingList<ABuyerVM> Customers = new BindingList<ABuyerVM>();
                context.Customers.Local.ToList().ForEach(x => Customers.Add(GetVMType(x)));
                return Customers;
            }
        }

        private ABuyerVM GetVMType(ABuyer cust)
        {
            if (cust is Teacher)
            {
                return new TeacherVM((Teacher)cust);
            }
            else
            {
                if(cust is Student)
                    return new StudentVM((Student)cust);
                else
                {
                    if(cust is NormalBuyer)
                        return new NormalBuyerVM((NormalBuyer)cust);
                }
            }

            return null;
        }

    }
}
