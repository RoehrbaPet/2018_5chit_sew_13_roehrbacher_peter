﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181220_Library
{
    public class OrdersVM
    {
        private LibraryContext context;
        public OrdersVM(LibraryContext context)
        { this.context = context; }
        

        public BindingList<OrderVM> Orders
        {
            get
            {
                context.Orders.Load();
                context.Articles.Load();
                context.Customers.Load();
                BindingList<OrderVM> orders = new BindingList<OrderVM>();
                context.Orders.Local.ToList().ForEach(x=> orders.Add(new OrderVM(x)));
                return orders;
            }
        }
    }
}
