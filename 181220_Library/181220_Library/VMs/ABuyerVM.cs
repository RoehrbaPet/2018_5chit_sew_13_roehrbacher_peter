﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using _181220_Library.Command;
using _181220_Library.VM;

namespace _181220_Library
{
    public abstract class ABuyerVM:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ABuyer customer;
        public UpdateCustomer UpdateCommand => new UpdateCustomer(Update);
        public DeleteCustomer DeleteCommand => new DeleteCustomer(Delete);
        public void Update()
        {
            Customer c = new Customer(customer);
            c.Show();
        }

        public void Delete()
        {
            LibraryContext.Library.Customers.Local.Remove(customer);
        }

       
        public int BuyerId
        {
            get
            {
                return customer.BuyerId;
                
            }
            set
            {
                customer.BuyerId = value;
                OnPropertyChanged();
            }
        }
        public string FirstName
        {
            get
            {
                return customer.FirstName;

            }
            set
            {
                customer.FirstName = value;
                OnPropertyChanged();
            }
        }
        public string LastName
        {
            get
            {
                return customer.LastName;

            }
            set
            {
                customer.LastName = value;
                OnPropertyChanged();
            }
        }

        public BindingList<Order> Orders
        {
            get { return customer.Orders.ToBindingList(); }
            set
            {
                customer.Orders = value.ToList();
                OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
