﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;


namespace _181220_Library.VM
{
    class SubjectVM:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public SubjectVM(Subject subject)
        {
            this.subject = subject;
        }

        protected Subject subject;

        public int SubjectId
        {
            get { return subject.SubjectId; }
            set
            {
                subject.SubjectId = value;
                OnPropertyChanged();
            }
        }

        public Teacher teacher
        {
            get { return subject.teacher; }
            set
            {
                subject.teacher = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return subject.Name; }
            set
            {
                subject.Name = value;
                OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
