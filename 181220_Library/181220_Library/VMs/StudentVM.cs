﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace _181220_Library
{
    class StudentVM:ABuyerVM,INotifyPropertyChanged
    {
        public StudentVM(Student student)
        {
            base.customer = student;
        }

        //TODO Change To SchoolVM
        public School school
        {
            get { return ((Student) base.customer).school;}
            set
            {
                ((Student) base.customer).school = value;
                OnPropertyChanged();
            }
        }

        public string ClassRoom
        {
            get { return ((Student) base.customer).ClassRoom; }
            set
            {
                ((Student) base.customer).ClassRoom = value;
                OnPropertyChanged();
            }
        }
        
        
    }
}
