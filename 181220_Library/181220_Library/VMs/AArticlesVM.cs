﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181220_Library.VM
{
    class AArticlesVM
    {
        private LibraryContext context;
        public AArticlesVM(LibraryContext context)
        { this.context = context; }


        public BindingList<AArticle> Articles
        {
            get
            {
                context.Orders.Load();
                return context.Articles.Local.ToBindingList();
            }
        }
    }
}
