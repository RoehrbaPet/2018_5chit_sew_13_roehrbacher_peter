﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181220_Library.VM
{
    class APublishersVM
    {
        private LibraryContext context;
        public APublishersVM(LibraryContext context)
        { this.context = context; }


        public BindingList<APublisherVM> Publishers
        {
            get
            {
                context.Orders.Load();
                context.Publishers.Load();
                context.Articles.Load();
                BindingList<APublisherVM> publishers = new BindingList<APublisherVM>();
                context.Publishers.Local.ToList().ForEach(x => publishers.Add(new APublisherVM(x)));
                return publishers;

            }
        }
    }
}
