﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _181207_Library
{
    class OrderVM:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public OrderVM(Order order)
        {
            this.order = order;
            
        }
        protected Order order;

        public int OrderId
        {
            get
            {
                return order.OrderId; 
            }
            set { order.OrderId = value; }

        }

        public DateTime OrderTime
        {
            get { return order.Date; }
            set
            {
                order.Date = value;
                OnPropertyChanged();
            }
        }

        public AArticle Article
        {
            get { return order.Article; }
            set
            {
                order.Article = value;
                OnPropertyChanged();
            }
        }

        public ABuyer Customer
        {
            get { return order.Customer; }
            set
            {
                order.Customer = value;
                OnPropertyChanged();
            }
        }


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
