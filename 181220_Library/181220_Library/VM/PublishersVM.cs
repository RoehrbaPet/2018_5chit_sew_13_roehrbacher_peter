﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181207_Library.VM
{
    class PublishersVM
    {
        private LibraryContext context;
        public PublishersVM(LibraryContext context)
        { this.context = context; }


        public BindingList<APublisher> Publishers
        {
            get
            {
                context.Orders.Load();
                return context.Publishers.Local.ToBindingList();
            }
        }
    }
}
