﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using _181207_Library.Annotations;

namespace _181207_Library
{
    abstract class ABuyerVM:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected ABuyer customer;

        public int BuyerId
        {
            get
            {
                return customer.BuyerId;
                
            }
            set
            {
                customer.BuyerId = value;
                OnPropertyChanged();
            }
        }
        public string FirstName
        {
            get
            {
                return customer.FirstName;

            }
            set
            {
                customer.FirstName = value;
                OnPropertyChanged();
            }
        }
        public string LastName
        {
            get
            {
                return customer.LastName;

            }
            set
            {
                customer.LastName = value;
                OnPropertyChanged();
            }
        }

        public BindingList<Order> Orders
        {
            get { return Orders; }
            set
            {
                Orders = value;
                OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
