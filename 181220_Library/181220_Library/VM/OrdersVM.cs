﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181207_Library
{
    public class OrdersVM
    {
        private LibraryContext context;
        public OrdersVM(LibraryContext context)
        { this.context = context; }
        

        public BindingList<Order> Orders
        {
            get
            {
                context.Orders.Load();
                return context.Orders.Local.ToBindingList();
            }
        }
    }
}
