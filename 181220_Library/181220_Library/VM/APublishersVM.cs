﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using _181207_Library.Annotations;

namespace _181207_Library
{
    class APublisherVM:INotifyPropertyChanged
    {
        public APublisherVM(APublisher publisher)
        {
            this.Publisher = publisher;
            
        }

        protected APublisher Publisher;

        public BindingList<AArticle> Articles
        {
            get { return Publisher.Articles.ToBindingList(); }
            set
            {
                Publisher.Articles = value.ToList();
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return Publisher.Name; }
            set
            {
                Publisher.Name = value;
                OnPropertyChanged();
            }
        }

        public int PublisherId
        {
            get { return Publisher.PublisherId; }
            set
            {
                Publisher.PublisherId = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
