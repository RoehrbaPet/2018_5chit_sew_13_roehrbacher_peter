﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181220_Library
{
    class AArticleRepository:AGenericRepository<AArticle>
    {

    }

    class SchoolRepository : AGenericRepository<School>
    {
        
    }
    class SubjectRepository : AGenericRepository<Subject>
    {

    }
    class TeacherRepository : AGenericRepository<Teacher>
    {

    }
    class StudentRepository : AGenericRepository<Student>
    {

    }
    class NormalBuyerRepository : AGenericRepository<NormalBuyer>
    {

    }
    class BuyerRepository : AGenericRepository<ABuyer>
    {

    }

    class AddressRepository : AGenericRepository<Address>
    {
        
    }

    class OrderRepository : AGenericRepository<Order>
    {
        
    }

    class PublisherRepository : AGenericRepository<APublisher>
    {
        
    }

}
