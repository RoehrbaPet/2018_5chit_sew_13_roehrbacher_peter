﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SongExample.Annotations;

namespace SongExample
{
    public class Song
    {
    }

    public class SongsVM
    {

        public BindingList<Song> Songs
        {
            get; set;
        }
        
    }

    public class SongVM:INotifyPropertyChanged
    {
        private Song songpr;
        public Song song
        {
            get { return songpr; }
            set
            {
                songpr = value;
                if(PropertyChanged != null)
                    OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
