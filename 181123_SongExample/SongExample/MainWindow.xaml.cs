﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SongExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SongsVM songs = new SongsVM();
            BindingList<Song> s = songs.Songs;
            s.Add(new Song("Hearteater", "Emil Bulls"));
            s.Add(new Song("The Pretender", "Foo Fighters"));
            s.Add(new Song("The day that I die", "Good Charlotte"));
            s.Add(new Song("Oracle", "Timmy Trumpet"));

            lbx_Songs.ItemsSource = s;
            
        }
    }
}
