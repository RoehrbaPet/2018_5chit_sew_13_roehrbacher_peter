﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181012_WPFCRUD
{
    public class CarContext : DbContext
    {
        public CarContext() : base("CarDB")
        {
            Database.SetInitializer(new CarDbInitializer());
        }

        public DbSet<Customer> Customers;
        public DbSet<Car> Inventory;
        public DbSet<CreditRisk> CreditRisks;
    }

    public class CarDbInitializer : DropCreateDatabaseAlways<CarContext>
    {
        protected override void Seed(CarContext context)
        {
            var Customers = new List<Customer>()
            {
                new Customer {FirstName = "Dave", LastName = "Brenner"},
                new Customer {FirstName = "Matt", LastName = "Walton"},
                new Customer {FirstName = "Steve", LastName = "Hagen"},
                new Customer {FirstName = "Pat", LastName = "Walton"},
                new Customer {FirstName = "Bad", LastName = "Customer"}
            };

            Customers.ForEach(x => context.Customers.Add(x));
            var cars = new List<Car>()
            {
                new Car {Make="VW", Color = "Black", PetName="Zippy"},
                new Car {Make="Ford", Color = "Rust", PetName="Zippy"},
                new Car {Make="Saab", Color = "Black", PetName="Zippy"},
                new Car {Make="Yugo", Color = "Yellow", PetName="Zippy"},
                new Car {Make="BMW", Color = "Black", PetName="Zippy"},
                new Car {Make="BMW", Color = "Green", PetName="Zippy"},
                new Car {Make="BMW", Color = "Pink", PetName="Zippy"},
                new Car {Make="BMW", Color = "Black", PetName="Zippy"},
                new Car {Make="Pinto", Color = "Black", PetName="Zippy"},
                new Car {Make="Yugo", Color = "Brown", PetName="Zippy"},
            };
            cars.ForEach(x => context.Inventory.Add(x));
            var orders = new List<Order>
            {
                new Order {Car = cars[0], Customer = Customers[0]},
                new Order {Car = cars[1], Customer = Customers[1]},
                new Order {Car = cars[2], Customer = Customers[2]},
                new Order {Car = cars[3], Customer = Customers[3]},

            };
            context.CreditRisks.Add(
                new CreditRisk()
                {
                    CustId = Customers[4].CustId,
                    FirstName = Customers[4].FirstName,
                    LastName = Customers[4].LastName
                });
            context.SaveChanges();
            base.Seed(context);

        }
    }
}
