using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Media;

namespace _181012_WPFCRUD
{ 
	public partial class Car
	{
		public override string ToString()
		{
			// Since the PetName column could be empty, supply
			// the default name of **No Name**.
			return $"{this.PetName ?? "**No Name**"} is a" +
                $" {this.Color} {this.Make} with ID {this.CarId}.";
		}

        [NotMapped]
	    public string MakeColor => $"{Make} + ({Color})";
	}
}